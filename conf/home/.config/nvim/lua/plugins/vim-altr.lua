vim.cmd([[
  noremap <C-t>f <Plug>(altr-forward)
  noremap <C-t>b <Plug>(altr-back)
]])

-- For ruby tdd
vim.fn['altr#define']('%.rb', 'spec/%_spec.rb')
-- For rails tdd
vim.fn['altr#define']('app/models/%.rb', 'spec/models/%_spec.rb', 'spec/factories/%s.rb')
vim.fn['altr#define']('app/controllers/%.rb', 'spec/controllers/%_spec.rb')
vim.fn['altr#define']('app/helpers/%.rb', 'spec/helpers/%_spec.rb')
