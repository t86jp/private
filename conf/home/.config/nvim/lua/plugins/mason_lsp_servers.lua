vim.cmd([[
  function! MasonLspServerInstall()
    MasonInstall typescript-language-server
    MasonInstall solargraph
    MasonInstall sql-formatter
    MasonInstall cspell
  endfunction
]])
