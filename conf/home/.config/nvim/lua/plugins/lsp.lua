-- ========== mappings
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  -- vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  -- vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  -- vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  -- vim.keymap.set('n', '<space>wl', function()
  --   print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  -- end, bufopts)
  -- vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  -- vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', function() vim.lsp.buf.format { async = true } end, bufopts)
end
-- ========== mappings end

-- ========== LSP Sever
-- vim.lsp.set_log_level("debug")
local lsp_flags = {
  -- This is the default in Nvim 0.7+
  debounce_text_changes = 150,
}

local lsp_config = require('lspconfig')
local capabilities = require('cmp_nvim_lsp').default_capabilities(
  vim.lsp.protocol.make_client_capabilities()
)
require('mason').setup()
require('mason-lspconfig').setup_handlers({ function(server)
  local opt = {
    capabilities = require('cmp_nvim_lsp').default_capabilities(
      vim.lsp.protocol.make_client_capabilities()
    ),
    on_attach = on_attach,
    flags = lsp_flags,
    root_dir = function() return vim.loop.cwd() end
  }
  lsp_config[server].setup(opt)
end })

lsp_config.solargraph.setup({
  capabilities = capabilities,
  on_attach = on_attach,
  flags = lsp_flags,
  root_dir = function() return vim.loop.cwd() end,
  settings = {
    solargraph = { diagnostics = false }
  }
})
-- ========== LSP Sever end

-- ========== cspell for null-ls
-- $XDG_CONFIG_HOME/cspell
local cspell_config_dir = '~/.config/cspell'
-- $XDG_DATA_HOME/cspell
local cspell_data_dir = '~/.local/share/cspell'
local cspell_files = {
  config = vim.call('expand', cspell_config_dir .. '/cspell.json'),
  dotfiles = vim.call('expand', cspell_config_dir .. '/dotfiles.txt'),
  vim = vim.call('expand', cspell_data_dir .. '/vim.txt.gz'),
  user = vim.call('expand', cspell_data_dir .. '/user.txt'),
}

-- vim辞書がなければダウンロード
if vim.fn.filereadable(cspell_files.vim) ~= 1 then
  local vim_dictionary_url = 'https://github.com/iamcco/coc-spell-checker/raw/master/dicts/vim/vim.txt.gz'
  io.popen('curl -fsSLo ' .. cspell_files.vim .. ' --create-dirs ' .. vim_dictionary_url)
end

-- ユーザー辞書がなければ作成
if vim.fn.filereadable(cspell_files.user) ~= 1 then
  io.popen('mkdir -p ' .. cspell_data_dir)
  io.popen('touch ' .. cspell_files.user)
end

-- ========== LSP Formatter For null-ls
local null_ls = require('null-ls')
null_ls.setup({
  debug = true,
  on_attach = on_attach,
  sources = {
    null_ls.builtins.formatting.jq,
    null_ls.builtins.formatting.sql_formatter.with({
      extra_args = {'--config', vim.fn.expand('~/.config/sql-formatter.json')}
    }),
    null_ls.builtins.diagnostics.rubocop.with({
      prefer_local = 'bundle_bin',
      extra_args = function(utils)
        return { '--config', '.rubocop.yml' }
      end,
      condition = function(utils)
        return utils.root_has_file({'.rubocop.yml'})
      end,
      extra_args = {
        '-c', '.rubocop.yml'
      },
    }),
    null_ls.builtins.diagnostics.yamllint,
    null_ls.builtins.formatting.rubocop.with({
      prefer_local = 'bundle_bin',
      extra_args = function(utils)
        return { '--config', utils.root_pattern({'.rubocop.yml'}) }
      end,
      condition = function(utils)
        return utils.root_has_file({'.rubocop.yml'})
      end,
      extra_args = {
        '-c', '.rubocop.yml'
      },
    }),
    null_ls.builtins.diagnostics.cspell.with({
      -- 起動時に設定ファイル読み込み
      extra_args = { '--config', cspell_files.config },
      diagnostics_postprocess = function(diagnostic)
        -- レベルをWARNに変更（デフォルトはERROR）
        diagnostic.severity = vim.diagnostic.severity["WARN"]
      end,
      condition = function()
        -- cspellが実行できるときのみ有効
        return vim.fn.executable('cspell') > 0
      end
    }),
  },
})
-- ========== LSP Formatter For null-ls end

-- ========== LSP Formatter For null-ls
require('mason-null-ls').setup({
  automatic_setup = false,
  ensure_installed = {'jq', 'prettier'},
  handlers = {
    function() end, -- disables automatic setup of all null-ls sources
    stylua = function(source_name, methods)
      null_ls.register(null_ls.builtins.formatting.stylua)
    end,
    shfmt = function(source_name, methods)
      -- custom logic
      require('mason-null-ls').default_setup(source_name, methods) -- to maintain default behavior
    end,
  },
})

-- ========== highlight
-- vim.cmd([[
--   set updatetime=500
--   highlight LspReferenceText  cterm=underline ctermfg=1 ctermbg=8 gui=underline guifg=#A00000 guibg=#104040
--   highlight LspReferenceRead  cterm=underline ctermfg=1 ctermbg=8 gui=underline guifg=#A00000 guibg=#104040
--   highlight LspReferenceWrite cterm=underline ctermfg=1 ctermbg=8 gui=underline guifg=#A00000 guibg=#104040
--   augroup lsp_document_highlight
--     autocmd!
--     autocmd CursorHold,CursorHoldI * lua vim.lsp.buf.document_highlight()
--     autocmd CursorMoved,CursorMovedI * lua vim.lsp.buf.clear_references()
--   augroup END
-- ]])
-- ========== highlight end

-- ========== diagnostic

-- vim.lsp.handlers['textDocument/publishDiagnostics'] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
--   update_in_insert = false,
--   virtual_text = {
--     format = function(diagnostic)
--       return string.format('%s (%s: %s)', diagnostic.message, diagnostic.source, diagnostic.code)
--     end,
--   },
-- })

require('toggle_lsp_diagnostics').init({
  update_in_insert = false,
  virtual_text = {
    format = function(diagnostic)
      return string.format('%s (%s: %s)', diagnostic.message, diagnostic.source, diagnostic.code)
    end,
  },
})
vim.cmd([[
nmap <leader>tld  <Plug>(toggle-lsp-diag)
]])
-- ========== diagnostic end
