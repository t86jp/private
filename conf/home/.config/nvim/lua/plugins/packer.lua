vim.cmd[[
  filetype off

  let s:plug_dir = $HOME. '/.local/share/nvim/site/pack/packer/start/'
  if has('vim_starting')
    let s:plug_vim_dir = s:plug_dir. '/packer.nvim'
    set rtp+=s:plug_vim_dir
    if !isdirectory(expand(s:plug_vim_dir))
      echo 'install packer...'
      call system('git clone --depth 1 https://github.com/wbthomason/packer.nvim '. s:plug_vim_dir)
      echo 'Please run :PackerInstall'
    end
  endif

  filetype plugin indent on
]]

vim.cmd[[packadd packer.nvim]]
