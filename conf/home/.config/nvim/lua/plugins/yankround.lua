vim.cmd([[
  nmap p <Plug>(yankround-p)
  nmap P <Plug>(yankround-P)
  nmap gp <Plug>(yankround-gp)
  nmap gP <Plug>(yankround-gP)
  "nmap <C-p> <Plug>(yankround-prev)
  "nmap <C-n> <Plug>(yankround-next)
  let g:yankround_max_history = 100
  "nnoremap <C-g><C-p> :<C-u>CtrlPYankRound<CR>
]])
