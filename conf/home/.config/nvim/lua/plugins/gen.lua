require('gen').setup({
  model = 'qwen2.5-coder:14b', -- The default model to use.

  quit_map = 'q', -- set keymap to close the response window
  retry_map = '<c-r>', -- set keymap to re-send the current prompt
  accept_map = '<c-cr>', -- set keymap to replace the previous selection with the last result
  host = 'localhost', -- The host running the Ollama service.
  port = '11434', -- The port on which the Ollama service is listening.
  display_mode = 'float', -- The display mode. Can be 'float' or 'split' or 'horizontal-split'.
  show_prompt = true, -- Shows the prompt submitted to Ollama.
  show_model = true, -- Displays which model you are using at the beginning of your chat session.
  no_auto_close = false, -- Never closes the window automatically.
  file = false, -- Write the payload to a temporary file to keep the command short.
  hidden = false, -- Hide the generation window (if true, will implicitly set `prompt.replace = true`), requires Neovim >= 0.10

  init = function(options) pcall(io.popen, 'ollama serve > /dev/null 2>&1 &') end,
  command = function(options)
    local body = { model = options.model, stream = true }
    return 'curl --silent --no-buffer -X POST http://'
    .. options.host
    .. ':'
    .. options.port
    .. '/api/chat -d $body'
    .. ' -H "Content-Type: application/json"'
  end,
  list_models = function(options)
    local response = vim.fn.systemlist('curl --silent --no-buffer http://' .. options.host .. ':' .. options.port .. '/api/tags')
    local list = vim.fn.json_decode(response)
    local models = {}
    for key, _ in pairs(list.models) do
      table.insert(models, list.models[key].name)
    end
    table.sort(models)
    return models
  end,
  -- The command for the Ollama service. You can use placeholders $prompt, $model and $body (shellescaped).
  -- This can also be a command string.
  -- The executed command must return a JSON object with { response, context }
  -- (context property is optional).
  -- list_models = '<omitted lua function>', -- Retrieves a list of model names
  debug = false -- Prints errors and the command which is run.
})

local complete_list = {
  'SelectModel'
}

vim.api.nvim_create_user_command(
  'GenOption',
  function(opts)
    require('gen').select_model()
  end,
  {
    nargs = 1,
    complete = function(_, _, _)
      return complete_list
    end
  }
)

vim.keymap.set({ "n", "v" }, "<leader>gl", ":Gen<CR>")
vim.keymap.set({ "n", "v" }, "<leader>gm", ":GenOption SelectModel<CR>")

require('gen').prompts = {
  Generate = { prompt = "$input", replace = true },
  Chat = { prompt = "$input" },
  Summarize = { prompt = "以下を要約してください:\n$text" },
  Ask = { prompt = "以下の文章について、 $input:\n$text" },
  Change = {
    prompt = "Change the following text, $input, just output the final text without additional quotes around it:\n$text",
    replace = true,
  },
  Enhance_Grammar_Spelling = {
    prompt = "Modify the following text to improve grammar and spelling, just output the final text without additional quotes around it:\n$text",
    replace = true,
  },
  Enhance_Wording = {
    prompt = "以下の文章をより良い表現に修正してください。最終テキストは引用符で囲まずに出力してください:\n$text",
    replace = true,
  },
  Make_Concise = {
    prompt = "以下の文章をできるだけシンプルで簡潔になるように修正してください。最終テキストは引用符で囲まずに出力してください:\n$text",
    replace = true,
  },
  Make_Markdown = {
    prompt = "マークダウンに変換してください :\n$text",
    replace = true,
  },
  Make_List = {
    prompt = "マークダウンリストに変換してください :\n$text",
    replace = true,
  },
  Make_Table = {
    prompt = "マークダウンテーブルに変換してください:\n$text",
    replace = true,
  },
  Review_Code = {
    prompt = "以下のコードをレビューし、簡潔な提案を行ってください:\n```$filetype\n$text\n```",
  },
  Enhance_Code = {
    prompt = "以下のコードを改良し、結果は```$filetype\n...\n```という形式で出力してください:\n```$filetype\n$text\n```",
    replace = true,
    extract = "```$filetype\n(.-)```",
  },
  Change_Code = {
    prompt = "以下のコードについて、$input、結果は```$filetype\n...\n```という形式で出力してください:\n```$filetype\n$text\n```",
    replace = true,
    extract = "```$filetype\n(.-)```",
  },
  Suggest_Naming_Code = {
    prompt = "変数名と関数名の名前を改善した提案リストと提案理由を作成してください。提案リストは提案前と提案名を表示し、日本語で説明してください:\n```$text```",
  },
  Translate_English = {
    prompt = " 以下の文章を英訳してください:\n$text"
  },
}
