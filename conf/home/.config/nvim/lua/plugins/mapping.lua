vim.cmd([[
nnoremap <D-d> "*d
nnoremap <D-d>d "*dd
vnoremap <D-d> "*d
vnoremap <D-d>d "*dd
nnoremap <D-y> "*y
nnoremap <D-y>y "*yy
vnoremap <D-y> "*y
vnoremap <D-y>y "*yy
nnoremap <D-p> "*p
vnoremap <D-p> "*p
]])
