require('plugins/packer')

require('packer').startup({function(use)
  use({'rose-pine/neovim', as = 'rose-pine' })
  use({'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' })

  -- use 'w0rp/ale'

  use 'scrooloose/nerdcommenter'

  -- camelize
  use 'kana/vim-operator-user'
  use 'tyru/operator-camelize.vim'

  use 'nvim-telescope/telescope.nvim'
  use 'kana/vim-altr'

  use 'nathanaelkane/vim-indent-guides'

  use 'itchyny/vim-parenmatch'
  use 'LeafCage/yankround.vim'

  -- lsp
  use 'neovim/nvim-lspconfig'
  use 'williamboman/mason.nvim'
  use 'williamboman/mason-lspconfig.nvim'
  use 'ray-x/lsp_signature.nvim'
  use 'WhoIsSethDaniel/toggle-lsp-diagnostics.nvim'

  use { 'jose-elias-alvarez/null-ls.nvim', requires = 'nvim-lua/plenary.nvim' }
  use 'jayp0521/mason-null-ls.nvim'

  -- auto complete
  use { 'hrsh7th/nvim-cmp', branch = 'main' }
  use { 'hrsh7th/cmp-nvim-lsp', branch = 'main' }
  use { 'hrsh7th/cmp-buffer', branch = 'main' }
  use { 'hrsh7th/cmp-path', branch = 'main' }
  use { 'hrsh7th/cmp-cmdline', branch = 'main' }
  use 'onsails/lspkind-nvim'
  use 'ray-x/cmp-treesitter'
  use 'folke/which-key.nvim'

  -- snippets
  use 'hrsh7th/vim-vsnip'
  -- use 'hrsh7th/vim-vsnip-integ'
  use 'rafamadriz/friendly-snippets'

  use 'lewis6991/gitsigns.nvim'

  use 'David-Kunz/gen.nvim'
  use 'github/copilot.vim'
  use { 'zbirenbaum/copilot-cmp', requires = 'zbirenbaum/copilot.lua' }
  use { 'CopilotC-Nvim/CopilotChat.nvim', requires = 'zbirenbaum/copilot.lua' }

  use {
    'yetone/avante.nvim',
    branch = 'main',
    run = 'make',
    requires = {
      -- Required plugins
      'nvim-treesitter/nvim-treesitter',
      'stevearc/dressing.nvim',
      'nvim-lua/plenary.nvim',
      'MunifTanjim/nui.nvim',
      'MeanderingProgrammer/render-markdown.nvim',

      -- Optional dependencies
      'hrsh7th/nvim-cmp',
      'nvim-tree/nvim-web-devicons', -- or use 'echasnovski/mini.icons'
      'HakonHarnes/img-clip.nvim',
      'zbirenbaum/copilot.lua',
    }
  }
end})

require('plugins/rose_pine')
require('plugins/treesitter')
require('plugins/nerdcommenter')
require('plugins/lsp')
require('plugins/operator')
require('plugins/plenary')
require('plugins/telescop')
require('plugins/vim-altr')
require('plugins/vim-indent-guides')
require('plugins/vim-operator-user')
require('plugins/vim-parenmatch')
require('plugins/yankround')
require('plugins/nvim-cmp')
require('plugins/snippets')
require('plugins/which-key')
require('plugins/gitsign')
require('plugins/gen')
require('plugins/copilot')
require('plugins/mapping')

require('plugins/mason_lsp_servers')
