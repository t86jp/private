local options = {
  nu = true,

  encoding = 'utf-8',
  fileformats = 'unix,dos',
  fenc = 'utf8',
  fileencoding = 'utf-8',
  fileencodings = 'utf-8,iso-2022-jp,euc-jp,ucs2le,ucs-2,cp932',

  syntax = enable,
  hlsearch = true,
  list = true,
  listchars = 'tab:>-,trail:-,extends:>,precedes:<',
  display = 'uhex',
  backup = false,
  mouse = '',

  laststatus = 2,
  cmdheight = 2,
  showcmd = true,
  title = true,

  -- 検索
  hlsearch = true,
  ignorecase = true,
  -- rtp+=/usr/local/opt/fzf

  -- 編集
  autoindent = true,
  smartindent = true,
  backspace = 'indent,eol,start',
  showmatch = true,
  wildmenu = true,
  -- formatoptions+=mM

  -- インデント
  tabstop = 2,
  shiftwidth = 2,
  softtabstop = 2,
  expandtab = true,
  smarttab = true,

  tags = './tags,./../tags,./*/tags,./../../tags,./../../../tags,./../../../../tags,./../../../../../tags',

  -- 辞書ファイルを使用する設定に変更
  -- complete+=k
}

vim.cmd([[
  filetype on
  filetype plugin on
  set statusline=%<%f\ %m%r%h%w%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%=%l,%c%V%8P
]])

for k, v in pairs(options) do
  vim.opt[k] = v
end
vim.opt.shortmess:append('c')


-- =================== ターミナル設定
-- tnoremap <silent> <ESC> <C-\><C-n>
-- :split term://sh -c 'git blame %'
-- command! t call s:indent_all()
