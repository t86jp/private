# Avanteルールファイル

## ルールファイルをコピーする

以下のコマンドを実行して、プロジェクト毎のルールファイルを作成してください。

```bash
cp custom.planning.avanterules.samples <PROJECT_DIR>/custom.planning.avanterules

```

## プロジェクト構造を作成

このプロジェクトについて以下の情報を調査してください。調査結果はテンプレートを参照して記入してください。
- プロジェクト内で使用されている技術スタックをPROJECT_TECHNOLOGY_STACKに記入
- プロジェクトのディレクトリ構造をディレクトリ構造例を参照してPROJECT_DIRECTORY_STRUCTUREに記入

調査結果テンプレート
```markdown
## Project Management Protocol

### 技術スタック
<PROJECT_TECHNOLOGY_STACK>

### プロジェクト構造規約
<PROJECT_DIRECTORY_STRUCTURE>

### 重要な制約
1. **特記事項**
   - データベーススキーマの参照は`db/schema.rb` で確認
2. **コード配置**
   - バックエンドのビジネスロジックは `app/services/` に配置
   - バックエンドのテストは `spec/` に配置
```

ディレクトリ構造例
```
my-next-app/
├── app/
│   ├── api/               # APIエンドポイント
│   ├── components/        # コンポーネント
│   │   ├── ui/            # 基本UI要素
│   │   └── layout/        # レイアウト
│   ├── hooks/             # カスタムフック
│   ├── lib/               # ユーティリティ
│   │   ├── api/           # API関連
│   │   └── utils/         # 共通関数
│   └── styles/            # スタイル定義
```

## プロジェクトのルールファイルを修正

プロジェクトのディレクトリ構造に合わせて、`custom.planning.avanterules` を修正してください。
