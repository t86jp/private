source ~/.zsh/base
source ~/.zsh/zinit

#######################################
# 基本
#######################################
export EDITOR=nvim
bindkey -d
bindkey -e # emacs mode

__use_rc_if_command_exists "$HOME/.zsh/fzf" "fzf"

__use_rc "$HOME/.zsh/env"
__use_rc "$HOME/.zsh/function"
__use_rc "$HOME/.zsh/alias"
__use_rc "$HOME/.zsh/completions"
#__use_rc "$HOME/.zsh/complete-ssh"
__use_rc "$HOME/.zsh/history"
__use_rc "$HOME/.zsh/prompt"
__use_rc "$HOME/.zsh/color"
__use_rc "$HOME/.zsh/navi"

#######################################
# 言語
#######################################
__use_rc "$HOME/.zsh/anyenv"
__use_rc "$HOME/.zsh/nodejs"
__use_rc "$HOME/.zsh/java"
__use_rc "$HOME/.zsh/go"
__use_rc "$HOME/.zsh/python"
__use_rc "$HOME/.zsh/perl"
# __use_rc "$HOME/.zsh/php"
__use_rc "$HOME/.zsh/ruby"

#######################################
# 開発
#######################################
__use_rc "$HOME/.zsh/docker"
__use_rc "$HOME/.zsh/android-studio"
# __use_rc_if_command_exists "$HOME/.zsh/peco" "peco"
# __use_rc_if_command_exists "$HOME/.zsh/percol" "percol"
__use_rc_if_command_exists "$HOME/.zsh/aws" "aws"

#######################################
# その他設定
#######################################
__use_rc "$HOME/.zsh/etc"

if [ -f "$HOME/.zshlocal" ]; then
  __use_rc "$HOME/.zshlocal"
fi

__use_rc "$HOME/.zsh/bind"
