if [ -f ~/perl5/perlbrew/etc/bashrc ]; then
  . ~/perl5/perlbrew/etc/bashrc
  export PERL_CPANM_OPT="--local-lib=~/.perl5"
  export PERL5LIB="$HOME/.perl5/lib/perl5:$HOME/.perl5/lib/perl5/i386-freebsd-64int:$HOME/.perl5/perlbrew/build/perl-5.16.3/lib/:$HOME/.perl5/perlbrew/perls/perl-5.16.3/lib/5.16.3/:$PERL5LIB"
fi

if [ -f /usr/local/bin/plenv ]; then
  eval "$(plenv init -)"
  export PATH="~/.plenv/shims:$PATH"
fi
