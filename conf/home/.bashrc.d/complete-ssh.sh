function __ssh_complete(){
  if [ ! -f "$HOME/.ssh/config" ]; then
    return 0
  fi

  local hosts=( $(grep '^Host ' "$HOME/.ssh/config" | awk '{print $2}') )
  local curw=${COMP_WORDS[COMP_CWORD]}
  COMPREPLY=( $(compgen -W '${hosts[@]}' -- $curw) )

  return 0
}
complete -F __ssh_complete ssh
complete -F __ssh_complete sftp
