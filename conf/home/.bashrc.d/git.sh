export GIT_PS1_SHOWDIRTYSTATE=0
export GIT_PS1_SHOWUNTRACKEDFILES=0
export GIT_PS1_SHOWSTASHSTATE=0
export GIT_PS1_SHOWUPSTREAM=0

if [ -f "/etc/bash_completion.d/git" ];then
  . /etc/bash_completion.d/git
else
  . /usr/local/Cellar/git/2.0.0/etc/bash_completion.d/git-prompt.sh
  . /usr/local/Cellar/git/2.0.0/etc/bash_completion.d/git-completion.bash
fi

__git_ps1_branch_name(){
  local current_branch=$(git branch 2>/dev/null | grep '*' | awk '{print $2}')
  if [ -n "$current_branch" ]; then
    echo " ($current_branch)"
  fi
}
