export HISTCONTROL="ignoredups"
percol-history() {
  local NUM=$(history | wc -l)
  local FIRST=$((-1*(NUM-1)))

  if [ $FIRST -eq 0 ] ; then
    # Remove the last entry, "percol-history"
    history -d $((HISTCMD-1))
    echo "No history" >&2
    return
  fi

  local CMD=$(fc -l $FIRST | sort -k 2 -k 1nr | uniq -f 1 | sort -nr | sed -E 's/^[0-9]+[[:blank:]]+//' | percol | head -n 1)

  if [ -n "$CMD" ] ; then
    # Replace the last entry, "percol-history", with $CMD
    history -s $CMD

    if type osascript > /dev/null 2>&1 ; then
      # Send UP keystroke to console
      (osascript -e 'tell application "System Events" to keystroke (ASCII character 30)' &)
    fi

    # Uncomment below to execute it here directly
    echo $CMD >&2
    eval $CMD
  else
    # Remove the last entry, "percol-history"
    history -d $((HISTCMD-1))
  fi
}


percol-command-snippet() {
  local snippet=$(grep -v "^#" ~/.percol-command-snippets | percol --query "$READLINE_LINE")
  if [ -z "$snippet" ]; then
    return 1
  fi

  snippet=$(echo "$snippet" | sed "s/^\[[^]]*\] *//g")
  if [ -z "$snippet" ]; then
    return 1
  fi
  eval "$snippet"
}
