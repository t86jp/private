__function_exists(){
  local command=$1
  type peco-history | grep -q 'function$' 2>/dev/null
  return $?
}

if __function_exists peco-history; then
  bind '"\C-r":"peco-history\n"'
else
  if __function_exists percol-history; then
    bind '"\C-r":"percol-history\n"'
  fi
fi

if __function_exists peco-command-snippet; then
  bind '"\C-f":"peco-command-snippet\n"'
else
  if __function_exists percol-command-snippet; then
    bind '"\C-f":"percol-command-snippet\n"'
  fi
fi
