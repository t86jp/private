__use_bashrc(){
  local rcfile=$1
  . $rcfile
}
__has_command(){
  local command=$1
  which $command 2>/dev/null
  return $?
}
__use_bashrc_if_command_exists(){
  local rcfile=$1
  local command=$2

  if __has_command $command; then
    __use_bashrc $rcfile
  fi
}

export IS_PLATFORM_MAC=0
export IS_PLATFORM_LINUX=0
export IS_PLATFORM_CYGWIN=0
if [ "$(uname)" == "Darwin" ]; then
  export IS_PLATFORM_MAC=1
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
  export IS_PLATFORM_LINUX=1
elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
  export IS_PLATFORM_CYGWIN=1
fi
