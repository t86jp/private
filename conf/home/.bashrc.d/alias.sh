alias vi='vim'
if [ $IS_PLATFORM_CYGWIN -eq "1" ]; then
  alias ls='ls --color=auto'
fi

if [ $IS_PLATFORM_MAC -eq "1" ]; then
  alias finder='open .'
fi

if __has_command vagrant; then
  alias vm='vagrant ssh -- -t'
fi
