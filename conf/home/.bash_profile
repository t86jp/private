if [ -f "${HOME}/.bashrc" ] ; then
  source "${HOME}/.bashrc"
fi

. "$HOME/.bashrc.d/base.sh"


__use_bashrc "$HOME/.bashrc.d/function.sh"
__use_bashrc "$HOME/.bashrc.d/alias.sh"
__use_bashrc "$HOME/.bashrc.d/path.sh"

__use_bashrc "$HOME/.bashrc.d/complete-ssh.sh"
__use_bashrc "$HOME/.bashrc.d/docker.sh"
__use_bashrc "$HOME/.bashrc.d/git.sh"
__use_bashrc "$HOME/.bashrc.d/java.sh"
__use_bashrc "$HOME/.bashrc.d/nodejs.sh"
__use_bashrc "$HOME/.bashrc.d/go.sh"
__use_bashrc_if_command_exists "$HOME/.bashrc.d/peco.sh" "peco"
__use_bashrc_if_command_exists "$HOME/.bashrc.d/percol.sh" "percol"
__use_bashrc "$HOME/.bashrc.d/python.sh"
__use_bashrc "$HOME/.bashrc.d/perl.sh"
__use_bashrc "$HOME/.bashrc.d/php.sh"
__use_bashrc "$HOME/.bashrc.d/ruby.sh"
__use_bashrc "$HOME/.bashrc.d/vagrant.sh"

__use_bashrc "$HOME/.bashrc.d/bind.sh"

export PS1='\[\033[32m\]\u\[\033[00m\]:\w\[\033[31m\]$(__git_ps1_branch_name)\[\033[00m\] \$ '

# Add environment variable COCOS_CONSOLE_ROOT for cocos2d-x
export COCOS_CONSOLE_ROOT=/Users/pk-22130005/test/cocos2d/cocos2d-x-3.2/tools/cocos2d-console/bin
export PATH=$COCOS_CONSOLE_ROOT:$PATH

# Add environment variable NDK_ROOT for cocos2d-x
export NDK_ROOT=/Applications/android-ndk-r10
export PATH=$NDK_ROOT:$PATH

# Add environment variable ANDROID_SDK_ROOT for cocos2d-x
export ANDROID_SDK_ROOT=/Applications/adt-bundle-mac-x86_64/sdk
export PATH=$ANDROID_SDK_ROOT:$PATH
export PATH=$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/platform-tools:$PATH

# Add environment variable ANT_ROOT for cocos2d-x
export ANT_ROOT=/usr/local/Cellar/ant/1.9.4/libexec/bin
export PATH=$ANT_ROOT:$PATH
