#!/usr/bin/env python

import os, os.path, sys, glob, re, socket

class VirtualHost:
  ReVM = re.compile(r'(?:\.?dev\.vm)*$')
  File = ''

  def __init__(self, host):
    self.host = self.to_vm_host(host)
  def to_vm_host(self, host):
    return self.__class__.ReVM.sub('.dev.vm', host)
  def save(self):pass


class WebServer(VirtualHost):
  ApacheDir = '/etc/apache2'
  LinkTo = '/'.join((ApacheDir, 'sites-enabled',))
  SaveTo = '/'.join((ApacheDir, 'sites-available',))

  @classmethod
  def template(klass):
    return '''
<VirtualHost *:80>
        ServerName %(NewServerName)s
        ServerAdmin webmaster@localhost

        DocumentRoot /usr/local/work/www/proj/%(NewServerName)s
        <Directory />
                Options Indexes +ExecCGI FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

        RewriteLog /var/log/apache2/%(NewServerName)s.rewrite.log
        RewriteLogLevel 3
        ErrorLog /var/log/apache2/%(NewServerName)s.error.log
        LogLevel warn

        CustomLog /var/log/apache2/%(NewServerName)s.access.log combined
        ServerSignature On
</VirtualHost>
'''

  def configuration(self):
    NewServerName = self.host
    return self.__class__.template() % locals()

  @classmethod
  def next_number(klass):
    numbers = [int(n[0]) for n in [os.path.basename(f).split('-') for f in glob.glob(klass.LinkTo + '/[0-9]*')]]
    numbers.sort()
    return '%03d' % (numbers[-1] + 1)

  def save(self):
    conf = '/'.join((self.__class__.SaveTo, self.host,)) + '.conf'
    w = open(conf, 'w')
    w.write(self.configuration())
    w.close

    link = '/'.join((self.__class__.LinkTo, '-'.join((self.__class__.next_number(), self.host,)), ))
    os.symlink(conf, link)

class Hosts(VirtualHost):
  File = '/etc/hosts'
  @classmethod
  def current_hosts_file(klass):
    r = open(klass.File, 'r')
    t = ''.join(r.readlines())
    r.close()
    return t
  def has_host(self):
    return not not re.search(self.host, self.__class__.current_hosts_file())

  def save(self):
    if(self.has_host()):
      return

    reg = re.compile(r'(#\s*(?:vm|work).*?)\n\n', re.S|re.I)
    ip = socket.gethostbyname(socket.gethostname())
    host = self.host.split('.',1)[0]

    new_hosts = reg.sub('\\1\n%s\t%s\t%s\n\n' % (ip, self.host, host), self.__class__.current_hosts_file(), 1)

    w = open(self.__class__.File, 'w')
    w.write(new_hosts)
    w.close()

def add_server(hostname):
  web = WebServer(hostname)
  hosts = Hosts(hostname)
  [c.save() for c in [web, hosts]]
  print 'added host: ', web.host

if(len(sys.argv) <= 1):
  print >> sys.stderr, 'argv error'
  sys.exit(1)
add_server(sys.argv[-1]);
