#!/bin/sh

reload_rc(){
  source ~/.zshrc
}


brew update
brew upgrade

brew install autoconf
brew install automake
brew install --cask cmake
brew install libtool
brew install git
brew install git-delta
brew install gh
brew install tig
brew install lazygit
#brew install vim
brew install neovim/neovim/neovim
brew install tmux
brew install w3m
brew install zsh
brew install zsh-completions
brew install gawk
brew install jq
brew install nkf

brew install --cask google-chrome
brew install --cask google-japanese-ime
# brew install --cask xquartz
# brew install --cask wireshark
# brew install --cask java
# brew install --cask android-sdk
# brew install --cask android-studio
# brew install gradle
# brew install --cask android-platform-tools
# brew install --cask genymotion
brew install --cask alacritty

# docker
#brew install docker
#brew install orbstack

brew install fzf
#$(brew --prefix)/opt/fzf/install
brew install ripgrep
brew install lsd
brew install fd
brew install hexyl
brew install navi

brew search '/font-.*-nerd-font/' | awk '{ print $1 }' | xargs -I{} brew install --cask {} || true

# Generative AI
# brew install --cask ollama
# ollama pull deepseek-coder-v2

#brew clean


## =================================================================

# alacritty
mkdir -p ~/.config/alacritty/themes
git clone https://github.com/alacritty/alacritty-theme ~/.config/alacritty/themes

# navi
navi repo add https://github.com/denisidoro/cheats
navi repo add denisidoro/navi-tldr-pages

# anyenv
git clone https://github.com/riywo/anyenv ~/.anyenv
reload_rc

[ -f /usr/local/opt/fzf/install ] && /usr/local/opt/fzf/install


# apm install plantuml-viewer
# apm install language-plantuml

sh -c "$(curl -fsSL https://raw.githubusercontent.com/zdharma/zinit/master/doc/install.sh)"

gh auth login
gh extension install github/gh-copilot
