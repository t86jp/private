#!/bin/bash

REPOS=$(pwd)

symlink() {
  local src=$1
  local dst=${2:-$(echo $src | sed -e "s@$REPOS/conf/home@$HOME@")}
  rm -f $dst
  ln -s $src $dst
}

symlink $REPOS/conf/home/.gitconfig
symlink $REPOS/conf/home/.gitconfig.d
symlink $REPOS/conf/home/.gitattributes
symlink $REPOS/conf/home/.hgrc
symlink $REPOS/conf/home/.inputrc
symlink $REPOS/conf/home/.my.cnf
symlink $REPOS/conf/home/.zsh
symlink $REPOS/conf/home/.zshrc
symlink $REPOS/conf/home/.ctags
symlink $REPOS/conf/home/.fzf.zsh
symlink $REPOS/conf/home/.myclirc
symlink $REPOS/conf/home/.tmux.conf
mkdir -p ~/.tmux/plugins/
symlink $REPOS/conf/home/.tmux/plugins/tpm
symlink $REPOS/conf/home/.tmux/status_line.conf
symlink $REPOS/conf/home/.config/nvim
symlink $REPOS/conf/home/.config/cspell
mkdir -p $HOME/Library/Application\ Support/lazygit
rm -f $HOME/Library/Application\ Support/lazygit/config.yml && ln -s $REPOS/conf/home/.config/lazygit/config.yml $HOME/Library/Application\ Support/lazygit/config.yml
symlink $REPOS/conf/home/.config/sql-formatter.json
symlink $REPOS/conf/home/.config/cspell
mkdir -p $HOME/.config/navi
symlink $REPOS/conf/home/.config/navi/config.yaml
mkdir -p ~/.local/share/navi/cheats
# symlink $REPOS/conf/home/.local/share/navi/cheats/...
mkdir -p $HOME/.config/alacritty
symlink $REPOS/conf/home/.config/alacritty/alacritty.toml
